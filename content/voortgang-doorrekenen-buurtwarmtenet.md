---
title: "Voortgang doorrekenen buurtwarmtenet"
date: 2021-06-19T07:51:38+02:00
url: "/voortgang-doorrekenen-buurtwarmtenet"
image: "img/warmtenetten.jpeg"
draft: false
description: "Op 19 mei heeft Greenvis zijn berekeningen gepresenteerd over drie mogelijke 'buurtwarmtenetten'. Deze drie opties waren in een vorig overleg geselecteerd uit zo'n 12 warmteoplossingen. De bedoeling is dat we na de zomer de uitkomsten van dit onderzoek gaan presenteren aan de buurt."

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Op 19 mei heeft Greenvis zijn berekeningen gepresenteerd over drie mogelijke 'buurtwarmtenetten'. Deze drie opties waren in een vorig overleg geselecteerd uit zo'n 12 warmteoplossingen. In dit stukje meer informatie over de volgende stappen.

<!--more-->

In [ons vorige stukje](https://klopvaartaardgasvrij.nl/onderzoek-naar-buurtwarmtenet/) hebben we deze drie opties kort toegelicht: 

* Zonthermie met hogetemperatuur seizoensopslag, aanvoertemp. 70 ºC: in plaats van gewone zonnepanelen zonnecollectoren op het dak en de warmte zoveel mogelijk opslaan voor gebruik in de winter.
* Aquathermie met centrale warmtepomp, aanvoertemp. 70 ºC. Bij aquathermie onderzoeken we of we warmte uit de Vecht of Klopvaart kunnen gebruiken. Die warmte moet dan wel weer opgewaardeerd worden tot een temperatuur waarmee wij ons huis en ons tapwater kunnen verwarmen.
* Bodemwarmtepomp met bodemlus: gezamenlijke bodemlus per cluster woningen, elke woning een eigen bodemwarmtepomp. Inmiddels is duidelijk dat deze optie wel mag in Utrecht, maar dan maximaal tot 50 meter diepte.

Uit de berekeningen van Greenvis komt naar voren dat zonthermie wat ruimtebeslag betreft bij ons in de Klopvaartbuurt niet goed mogelijk is. Het blijft natuurlijk een mooie oplossing: zonnewarmte in de zomer opslaan om in de winter je huis mee te verwarmen. Aquathermie en de bodemwarmtepomp blijven over. De bodemwarmtepomp is het meest duurzaam, maar ook het meest ingrijpend: we zullen onze huizen allemaal goed moeten isoleren en er komt veel apparatuur op je zolder te staan. Als kernteam hebben we hier nog eens goed over gesproken, maar wij verwachten dat een deel van onze buurt zo'n ingrijpende oplossing niet ziet zitten. 

# Aquathermie
Blijft over aquathermie. Daarbij gebruiken we warmte uit de Vecht, niet uit de Klopvaart. Berekeningen laten zien dat de Klopvaart te weinig warmte kan leveren. Voorlopig is het idee om de gehele Klopvaartbuurt (dus inclusief sociale huur), de Vechtzoombuurt (Costa Ricadreef) en het Antoniuskwartier (bij het zwembad) met één aquathermie-warmtenet te gaan bedienen. Ons kernteam heeft wel enkele zorgen geuit over deze oplossing. Door de schaalgrootte is het idee van een buurtcoöperatie moeilijk haalbaar (de buurten liggen ver uit elkaar en hebben minder (sociale) binding met elkaar). Dat betekent dat we dan toch in de armen worden gedreven van een exploitant die zich kan gaan gedragen als monopolist (denk aan stadsverwarming en Eneco). Op dit moment voelen wij daar weinig voor. 

# Wat is nu de volgende stap? De businesscase
Greenvis gaat aquathermie uitwerken in een zogenaamde businesscase. Dus in meer detail de kosten en baten in beeld brengen. Uiteindelijk gaat het de gemeente er om dat zij weten hoeveel geld erbij moet om te kunnen garanderen dat het ons geen cent extra gaat kosten. Uit die business case moet duidelijk worden wat de kosten zijn van het aanleggen en aansluiten van zo'n warmtenet, maar ook wat de kosten zijn om ons daarmee warm te houden. 

# Er is meer: all electric, stadsverwarming
Tegelijk met dit buurtwarmtenet onderzoekt Greenvis de mogelijkheid van het doortrekken van stadsverwarming naar de Klopvaartbuurt en wat het kost om met een eigen warmtepomp van het gas af te gaan (als je niet mee wilt doen met het buurtwarmtenet of niet op stadsverwarming aangesloten wilt worden). Al deze berekeningen moeten ons uiteindelijk inzicht gaan geven in of deze oplossingen aan onze voorwaarden kunnen voldoen. 

# Communicatie naar de buurt
De bedoeling is dat we na de zomer de uitkomsten van dit onderzoek gaan presenteren aan de buurt. Wij hopen meerdere bijeenkomsten te kunnen organiseren in de Stadstuin. Wij vinden het erg belangrijk dat zoveel mogelijk bewoners ook echt langs komen, wij willen van zoveel mogelijk bewoners van onze buurt de mening horen. Wij denken erover om de deuren langs te gaan en buren uit te nodigen. Dat kunnen wij natuurlijk niet alleen. Na de zomer komen we met een plan. Voor de zomer zullen we nog wel een terugkoppeling geven op de business cases zoals ze door Greenvis zijn doorgerekend.
