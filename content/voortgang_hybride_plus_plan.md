---
title: "Voortgang uitwerking hybride-plus plan als alternatief voor stadsverwarming"
date: 2022-12-12T07:51:38+02:00
url: "/voortgang_hybride_plus_plan"
image: "img/klopvaart3.jpeg"
draft: false
description: "De kernteams van Klopvaartbuurt Aardgasvrij en Nieuwe Energie voor de Vechtzoom (NEV) zijn druk bezig met het uitwerken van een alternatief voor de warmteoplossing van de gemeente (stadsverwarming). Eerder hebben we al laten weten dat wij serieuze bedenkingen hebben bij de stadsverwarming als 'aardgasvrije' oplossing. Tijdens een raadsinformatiebijeenkomst heeft Gerbert Hengelaar (NEV) een gloedvol betoog gehouden over de onzekere verduurzamingsstrategie van Eneco. Dat heeft de interesse van veel gemeenteraadsfracties gewekt. 
Inmiddels hebben we met de belangrijkste raadsfracties gesproken en lijkt er wat beweging te komen om meer aandacht te geven aan de alternatieven die wij voorstellen."

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

De kernteams van Klopvaartbuurt Aardgasvrij en Nieuwe Energie voor de Vechtzoom (NEV) zijn druk bezig met het uitwerken van een alternatief voor de warmteoplossing van de gemeente (stadsverwarming). Eerder hebben we al laten weten dat wij serieuze bedenkingen hebben bij de stadsverwarming als 'aardgasvrije' oplossing. Tijdens een raadsinformatiebijeenkomst heeft Gerbert Hengelaar (NEV) een gloedvol betoog gehouden over de onzekere verduurzamingsstrategie van Eneco. Dat heeft de interesse van veel gemeenteraadsfracties gewekt. 
Inmiddels hebben we met de belangrijkste raadsfracties gesproken en lijkt er wat beweging te komen om meer aandacht te geven aan de alternatieven die wij voorstellen. 

Het belangrijkste alternatief is het 'Hybride-Plus' plan. Daarbij zetten wij als bewoners zoveel mogelijk in op een hybride warmtepomp en isolatie, om zo snel mogelijk onze CO2-uitstoot te beperken. Waar mogelijk kiezen bewoners er al voor om volledig van het gas af te gaan met een warmtepomp. Wij zijn in gesprek met leveranciers die met ons mee willen denken over bijvoorbeeld de financiering (betaalbaarheid!) en ook over geluidsarme oplossingen. 
Natuurlijk kunnen we alleen voor een hybride oplossing kiezen als de deadline voor gasloos (2030) wordt uitgesteld. En dat is precies wat we in gesprek met de raadsfracties willen bereiken. 
Voorjaar 2023 hopen we ons plan af te maken en aan de bewoners voor te leggen. Als er een duidelijke meerderheid voor dit plan te vinden is, gaan we met dit plan naar de gemeenteraad. Wordt vervolgd. 
