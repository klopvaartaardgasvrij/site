---
title: "Alternatieven voor stadsverwarming: hybride warmtepomp of mini-warmtenet?"
date: 2022-09-12T07:51:38+02:00
url: "/hybride-warmtepomp-of-mini-warmtenet"
image: "img/warmteoplossingen.jpg"
draft: false
description: "Gezien de gasprijzen en de beperkte beschikbaarheid van het gas, willen we een alternatief bieden voor de stadsverwarmingsplannen van de gemeente. Met dat alternatief willen we op korte termijn veel winst boeken wat duurzaamheid en gasverbruik betreft. (De stadsverwarming draait nu nog grotendeels op aardgas.). Dat alternatief kan een hybride warmtepomp zijn. "

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Gezien de gasprijzen en de beperkte beschikbaarheid van het gas, willen we een alternatief bieden voor de stadsverwarmingsplannen van de gemeente. Met dat alternatief willen we op korte termijn veel winst boeken wat duurzaamheid en gasverbruik betreft. (De stadsverwarming draait nu nog grotendeels op aardgas.). Dat alternatief kan een hybride warmtepomp zijn.

<!--more-->

## Hybride warmtepomp?

Zo'n warmtepomp kunnen we combineren met onze CV-ketels en daarmee tot wel 70% van het gasverbruik besparen. Onze elektriciteitsrekening gaat dan natuurlijk wel omhoog. Een hybride warmtepomp levert een groot deel van het jaar voldoende warmte, alleen op echt koude dagen moet de CV-ketel bijspringen.Een volgende stap is om onze woningen zo te isoleren dat het gas niet meer nodig is voor koude pieken. Zodat we op termijn geen aardgas meer nodig hebben om te verwarmen. Dit betekent wel dat we een langere periode nodig hebben om over te schakelen. Dus niet in 2030 maar uiterlijk in 2040. Dat geeft ook meer tijd om alle woningen goed te isoleren. Om als buurt uitstel te krijgen moeten we een plan hebben met brede steun en bereidheid tot deelname van onze bewoners. En moeten de resultaten voor duurzaamheid beter zijn dan wat de stadsverwarming gaat bieden.Op dit moment zijn we deze optie samen met Nieuwe Energie voor de Vechtzoom aan het uitwerken. De resultaten zullen we met u delen. Tegen die tijd (waarschijnlijk einde dit jaar) zullen we ook weer bijeenkomsten organiseren en over dit onderwerp met u in gesprek gaan.

## Mini-warmtenet

Nog een alternatief voor stadsverwarming: is het mogelijk om een blokje huizen (vaak 6 huizen in één blok in onze buurt) met één gezamenlijke warmtepomp te verwarmen? Ook dit is een aanpak die we op dit moment onderzoeken. Op de Vancouverdreef is er een huizenblok dat interesse heeft getoond voor deze aanpak. Wordt vervolgd. 

