---
title: "Brief naar de gemeenteraad over ‘Warmtepomp voor iedereen’"
date: 2023-10-20T16:51:38+02:00
url: "/Brief_gemeenteraad_Warmtepomp_voor_iedereen"
image: "img/hand-1917895_1280.png"
draft: false
description: "De afgelopen tijd zijn de kernteams van Klopvaartbuurt Aardgasvrij (KVA) en Nieuwe Energie voor de Vechtzoom (NEV) druk geweest een alternatief plan te bedenken voor het aardgasvrij maken van de Klopvaartbuurt en Vechtzoombuurt. De gemeente stuurt aan op aansluiting op de stadsverwarming. In ons plan ‘Warmtepomp voor iedereen’ willen we onze huizen met een goede warmtepomp (PVT-warmtepomp) en goede isolatie van het gas af halen. "

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

De afgelopen tijd zijn de kernteams van Klopvaartbuurt Aardgasvrij (KVA) en Nieuwe Energie voor de Vechtzoom (NEV) druk geweest een alternatief plan te bedenken voor het aardgasvrij maken van de Klopvaartbuurt en Vechtzoombuurt. De gemeente stuurt aan op aansluiting op de stadsverwarming. In ons plan ‘Warmtepomp voor iedereen’ willen we onze huizen met een goede warmtepomp (PVT-warmtepomp) en goede isolatie van het gas af halen. Hierover hebben wij een brief naar de gemeenteraad gestuurd.

<!--more-->

Belangrijke overwegingen daarbij zijn dat we niet afhankelijk worden van één warmteleverancier (Eneco), dat de kosten iets gunstiger uitkomen dan met stadsverwarming en we een duurzamere oplossing willen. Wij hebben in juni informatiebijeenkomsten georganiseerd, waar 20% van de huiseigenaren op af is gekomen en hun mening over ons plan heeft gegeven. 90% van deze groep huiseigenaren ziet perspectief in ‘Warmtepomp voor iedereen’. Voor meer informatie zie [hier](https://klopvaartaardgasvrij.nl/buurtbijeenkomsten-warmtepomp-voor-iedereen-succes/) een al eerder verspreid verslag van deze bijeenkomsten. 

In oktober heeft de wethouder een brief naar de raad gestuurd. In de media is daar aandacht aan besteed: overstappen op stadsverwarming kan nu nog niet zonder hoge aansluitkosten voor de bewoners. De gemeente wil een gratis overstap naar stadswarmte voor alle bewoners in Overvecht-Noord. Daarom stellen ze de plannen uit totdat deze kosten met subsidie naar nul kunnen gaan.

Wat niet in de media stond vermeld, is dat de wethouder in deze brief uitgebreid in gaat op ons plan ‘Warmtepomp voor iedereen’ (zie de [brief van de gemeente aan de raad](https://klopvaartaardgasvrij.gitlab.io/site/files/Raadsbrief%20Tussenrapportage%20uitvoeringsprogramma%20Overvecht-Noord%20Aardgasvrij%20en%20ontwikkelingen.pdf), vanaf pagina 3). Wij hebben aan de gemeente duidelijkheid gevraagd of ze ons plan willen steunen en op pagina 4 komt het hoge woord eruit: “We kiezen er op dit moment daarom voor om ons voorkeursalternatief van stadswarmte verder uit te werken. Aangezien de buurtinitiatieven het met ons eens zijn dat de plannen niet naast elkaar kunnen bestaan, betekent dit automatisch dat de gemeentelijke financiële steun is voorbehouden aan het gemeentelijke plan.” Vervolgens legt de wethouder uit waarom haar plan voor stadverwarming zo’n geweldig plan is en gaat maar beperkt in op ons plan zelf. Overigens heeft de gemeente ons plan laten analyseren door een adviesbureau en dit bureau heeft geconcludeerd dat ‘Warmtepomp voor Iedereen’ wat kosten betreft inderdaad kan concurreren met de stadsverwarming. 

Als buurtinitiatieven zijn wij natuurlijk teleurgesteld over het afwijzende besluit van de gemeente. Wij hebben besloten om de gemeenteraad met een brief te informeren waarom wij denken dat dit besluit een gemiste kans is voor de gemeente (en natuurlijk vooral voor ons). Deze brief vindt u [hier](https://klopvaartaardgasvrij.gitlab.io/site/files/Brief_gemeenteraad_NEV-KVA_plan_Warmtepomp_voor_iedereen_20-11-2013_DEF.pdf). Met deze brief zijn 6 bijlagen verzonden ter onderbouwing. Mocht u die willen bekijken, dan vindt u ze hier: [de brief](https://klopvaartaardgasvrij.gitlab.io/site/files/brief_bijlagen_gemeenteraad/Bijlage_1_Brief.pdf), [de samenvatting van het plan](https://klopvaartaardgasvrij.gitlab.io/site/files/brief_bijlagen_gemeenteraad/Bijlage_2_Samenvatting_plan_Warmtepomp_voor_iedereen_gemeenteraad_20-11-2023_DEF.pdf), [de onderbouwing van het plan](https://klopvaartaardgasvrij.gitlab.io/site/files/brief_bijlagen_gemeenteraad/Bijlage_3_Onderbouwing_plan_Warmtepomp_voor_iedereen.pdf), [de rapportage van de buurgesprekken en enquete](https://klopvaartaardgasvrij.gitlab.io/site/files/brief_bijlagen_gemeenteraad/Bijlage_4_Rapportage_buurtgesprekken_enquete_juni_2023.pdf), [de netcongestie-analyse](https://klopvaartaardgasvrij.gitlab.io/site/files/brief_bijlagen_gemeenteraad/Bijlage_5_Netcongestie_analyse.pdf), [de externe validatie van onze business case](https://klopvaartaardgasvrij.gitlab.io/site/files/brief_bijlagen_gemeenteraad/Bijlage_6_Externe_validatie_businesscase_DWTM.pdf) en [onze standpuntnota over standsverwarming](https://klopvaartaardgasvrij.gitlab.io/site/files/brief_bijlagen_gemeenteraad/Bijlage_7_standpuntnota_stadsverwarming.pdf)

Wat wij in [onze brief](https://klopvaartaardgasvrij.gitlab.io/site/files/Brief_gemeenteraad_NEV-KVA_plan_Warmtepomp_voor_iedereen_20-11-2013_DEF.pdf) concreet van de raadsleden vragen is: 
1. Principetoezegging dat ons plan bij steun van een ruime meerderheid van de bewoners het warmtealternatief voor onze buurten kan worden en de gemeente actief gaat meewerken.
2. Principe toezegging dat ons plan met een vergelijkbare aanvullende subsidie per woning vanuit de gemeente wordt gesteund, als de gemeente in Overvecht-Noord per woning beschikbaar stelt voor de business case van aansluiting op de stadsverwarming.
3. Principetoezegging dat de gemeente wil meewerken aan een aanvullende en ontzorgende financieringsoplossing, zodat de investering voor iedereen haalbaar is (uiteraard binnen de basisafspraak dat de totale subsidie/financiële steun vergelijkbaar is met de aansluiting op  stadsverwarming).
4. Afspraak dat we in de komende consultatie over aardgasvrij in Overvecht-Noord beide plannen aan onze buurten voorleggen en tot een gezamenlijke stemming/draagvlakpeiling komen.

Via onze contacten met raadsleden weten wij dat de raad onze brief en vragen waarschijnlijk wil bespreken. Wij hopen dat de gemeenteraad de wethouder wil oproepen om ons alternatief voor onze buurt te gaan steunen. Mocht er informatie zijn over raadsbijeenkomsten waarin onze brief en ons plan worden besproken, dan delen wij dat via onze mailing. 