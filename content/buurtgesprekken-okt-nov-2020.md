---
title: "Buurtgesprekken eind 2020: aanleiding, presentatie & vragen uit de buurt"
date: 2020-11-15T15:51:38+02:00
url: "/buurtgesprekken-okt-nov-2020"
image: "img/gesprek.png"
draft: false
description: "In oktober en november vonden drie (online) buurtgesprekken plaats. Klik hieronder om te lezen waarom we deze avonden georganiseerd hebben en de presentatie vanuit het kernteam, waarin we de buurtbewoners bijpraatten en aangaven wat wij denken dat er nu moet gebeuren rond Overvecht-Noord Aardgasvrij en onze buurt. Ook gaven we een samenvatting van de vragen die gesteld worden door bewoners, met de kijk van het kernteam op deze vragen."

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

In oktober en november vonden drie (online) buurtgesprekken plaats. Klik hieronder om te lezen waarom we deze avonden georganiseerd hebben en de presentatie vanuit het kernteam, waarin we de buurtbewoners bijpraatten en aangeven wat wij denken dat er nu moet gebeuren rond Overvecht-Noord Aardgasvrij en onze buurt. Ook geven we een samenvatting van de vragen die gesteld worden door bewoners, met de kijk van het kernteam op deze vragen.

<!--more-->

# Aanleiding buurtgesprek

De afgelopen tijd heeft het buurtinitiatief Klopvaartbuurt Aardgasvrij zich intensief bemoeid met het onderzoek van CE Delft naar de beste warmteoplossingen voor Overvecht-Noord. Inmiddels is dat onderzoek afgerond. Samen met het buurtinitiatief Nieuwe Energie Vechtzoom hebben we een zogenaamde ‘standpuntnota’ opgesteld (lees de standpuntnota via [deze link](https://klopvaartaardgasvrij.gitlab.io/site/files/Standpuntnota.pdf)). In deze nota evalueren we de conclusies uit het rapport van CE Delft en geven we aan hoe we verder willen.

Het rapport van CE Delft levert ons een aantal concrete conclusies op. Zo blijkt dat een aantal oplossingen niet aan onze voorwaarden voldoen. Anderen oplossingen lijken wel wat te bieden te hebben. Het rapport levert echter te weinig informatie om te stellen dat er oplossingen zijn die aan onze voorwaarden voldoen. Zo zijn de te verwachten kosten globaal en niet per huis berekend. Subsidies zijn niet doorgerekend. Ook op het gebied van duurzaamheid en comfort zijn er nog veel vraagtekens van onze kant.

De gemeente is van plan om binnenkort de wijk in te gaan om met bewoners in gesprek te gaan. Wij vinden het van belang dat wij voor die tijd de achtergrondinformatie delen die we op hebben opgedaan in de sessies met CE Delft.

Daarom hebben we de buurtbewoners uitgenodigd om een online meeting bij te wonen. Er waren drie online meetings om iedereen de kans te geven om mee te doen.

# Presentatie van het kernteam Klopvaartbuurt Aardgasvrij

Tijdens de meeting hebben we de buurtbewoners teruggekoppeld wat wij vinden van het rapport van CE Delft. Ook leggen we uit wat wij denken dat er nu moet gebeuren rond Overvecht-Noord Aardgasvrij en onze buurt. Klik [hier](https://klopvaartaardgasvrij.gitlab.io/site/files/buurtgesprekken-okt-nov-2020-presentatie.pdf) om de presentatie te bekijken.

[![Presentatie: wat gaan we doen?](https://klopvaartaardgasvrij.gitlab.io/site/img/presentatie-wat-gaan-we-doen.jpg)](https://klopvaartaardgasvrij.gitlab.io/site/files/buurtgesprekken-okt-nov-2020-presentatie.pdf)

# Vragen uit de buurt

Natuurlijk waren de avonden niet alleen bedoeld om te informeren, maar ook om te horen hoe onze buurtbewoners denken over de ontwikkelingen en welke vragen er leven in de buurt over dit proces. Hieronder zijn de gestelde vragen op de drie avonden op onderwerp verzameld. Per onderwerp geven we onze kijk op deze vragen.

## Procesmatig

1.	Wanneer deadline van gemeente?
2.	Kunnen we niet wachten ivm technische ontwikkelingen?
3.	Kunnen we niet wachten ivm goedkopere oplossingen?
4.	Wat is onze inspraak precies?
5.	Kunnen we de gasleidingen niet ook gebruiken voor H2?

De gemeente heeft een ambitieus plan waarin ze medio 2021 een knoop door willen hakken over de oplossing voor het aardgasvrij maken van onze wijk. Wij denken dat dit plan te ambitieus is en dat het nog wel iets langer gaat duren. In de herfst (we verwachten november 2020) komt de gemeente middels een campagne naar de wijk om uitleg te geven over het proces en over de alternatieven voor aardgas. We verwachten dat dit minimaal een brief is waarin staat uitgelegd wat de gemeente wil, wanneer ze dit willen en hoe ze dit willen. Wij zullen de gemeente scherp houden op onze voorwaarden voor het overstappen op een gasloze oplossing. Ons uitgangspunt is te allen tijde de voorwaarden zoals we die opgesteld hebben en waar we allemaal voor getekend hebben.

Eén van de voorwaarden is betaalbaar. Sterker nog, we hebben de voorwaarde heel erg scherp gesteld. Namelijk: niet duurder dan het gebruik van aardgas nu. Mocht er een oplossing zijn die hieraan (en ook de andere voorwaarden) voldoet dan zullen we positief reageren naar de gemeente. We verwachten niet dat er oplossingen komen die goedkoper worden dan aardgas, ook niet als we langer wachten. Een voordeel van het meegaan in een proeftuin is ook dat er hoogstwaarschijnlijk meer subsidie vrij kan komen voor de transitie. De gemeente heeft er ook baat bij dat deze transitie goed verloopt en dat wij tevreden zijn.

Onze inzet was om, als buurtinitiatief, bij de gemeente aan tafel te zitten om op deze manier zeggenschap af te dwingen over wat er met onze huizen gaat gebeuren. Mede doordat iedereen de handtekening gezet heeft en zich achter onze voorwaarden heeft geschaard zitten we nu daadwerkelijk bij de gemeente aan tafel en hebben we inspraak. Het blijft de gemeente die verantwoordelijk is voor de geboden oplossing. Zij gaan ook met elke huiseigenaar apart in gesprek voor een geboden oplossing. Wij zitten vooral aan tafel om de geboden oplossingen te toetsen aan onze voorwaarden.

We zijn zeker naar meerdere oplossingen aan het kijken en niet enkel naar het (buurt)warmtenet. H2 (waterstofgas) is hoogstwaarschijnlijk niet genoeg voorradig om ingezet te kunnen worden voor de verwarming van huizen uit de jaren 80 en nieuwer. Met name, omdat deze huizen goed te isoleren zijn en het H2 gebruikt gaat worden voor vervoer, industrie en panden die slecht te isoleren zijn (monumentale panden bijvoorbeeld). Maar we houden de ontwikkelingen zeker in de gaten. We willen het gasleidingennetwerk ook liever niet weg hebben. Waar mogelijk kan het blijven liggen om gebruikt te worden voor een alternatief voor aardgas. Hetzelfde verhaal gaat ook op voor groen gas of biogas. Daarvan is er ook nog niet genoeg voorradig.

## Isoleren

1.	Stijging warmtevraag is toch een reductie in isolatiebehoefte. (Ook andersom!)
2.	Hoe dan ook zullen we met z’n allen aan de isolatie moeten. De warmtevraag moet naar beneden! Kan de gemeente niet meedenken/meehelpen in het organiseren van isoleren in de buurt?

Zeker! Hoe meer warmte je vraagt, hoe minder de isolatie hoeft te zijn. We zien het ook vooral andersom, een betere isolatie van de woning is een reducering in de warmtevraag. Isoleren is dus altijd goed. De enige reden om nog even te wachten met het isoleren is omdat je misschien nog niet weet welk isolatieniveau er nodig is voor de transitie. Het zou zonde zijn om je woning te isoleren en er dan later achter te komen dat je nog meer zou moeten doen. Teveel is vaak niet erg, omdat het ook weer werkt als een reducering in je warmtevraag en dus in een lagere maandlasten van je warmte.

We zijn met de gemeente in gesprek over het isoleren. Naast het onderzoeken wat een goed alternatief voor aardgas is, willen we dat de gemeente ons ook tegemoet komt in het isoleren van onze woningen. Dat gaat ook zeker meehelpen in de waardestijging van de woningen. Linksom of rechtsom zal er geïsoleerd moeten worden.

## Warmtenet

1.	Hoe groot?
	a.	Alleen Klopvaart of groter?
2.	Hoe betalen?
	a.	Investeringskosten, zijn die te lenen bij de bank door een buurt of kleine coöperatie ipv een groot Eneco?
	b.	Hoe gaan de betalingen?
3.	Is de IJsbaan een bron?

Er zijn, uiteraard, veel vragen over de technische aspecten van een eventueel (buurt)warmtenet. In de Zoommeeting van donderdag 29 oktober is ook heel goed door iemand aangegeven dat de discussie bemoeilijkt wordt doordat er nog veel aspecten onbekend zijn en uitgezocht moeten worden. Alle vragen hierboven zullen zeker in het onderzoek naar voren komen en beantwoord worden. De IJsbaan is waarschijnlijk geen geschikte bron voor warmte, omdat de restwarmte van lage temperatuur is en opgewaardeerd moet worden. Ook zal er een flink transport van de warmte zijn van de IJsbaan helemaal naar bijvoorbeeld de natuurtuin. Dit transport kan efficiënt, maar er is altijd warmteverlies, dus is er weer meer opwaardering nodig.

We zijn blij met alle opties van warmtebronnen die genoemd worden, we kijken zelf ook naar bijvoorbeeld de Klopvaart, de Vecht, de Waterzuivering, de Jumbo, etc. Het is alleen zeer lastig. Een goede warmtebron moet voldoen aan veel eisen wil het een geschikte warmtebron zijn. Ook wordt er gekeken naar de warmteopslag. In de zomer wil je warmte verzamelen, opslaan en in de winter moet deze warmte vrijkomen en onze huizen verwarmen. Een zeer ingewikkeld proces waarbij wij de vragen gaan stellen en de experts dit gaan uitzoeken en uitleggen. Wederom houden wij onze voorwaarden in de hand om te toetsen of de geboden oplossingen voldoen aan de voorwaarden.
