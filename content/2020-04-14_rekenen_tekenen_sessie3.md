---
title: "Derde reken- en tekensessie met CE Delft"
date: 2020-05-17T20:50:38+02:00
url: "/2020-04-14_rekenen_tekenen_sessie3"
image: "img/20200414_eindgebruikerskosten.jpg"
draft: false
description: "werksessie 3"
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Op 14 april vond de derde en laatste reken- en tekensessie plaats met CE Delft. De presentaties van en onze op- en aanmerkingen over deze bijeenkomsten zijn hieronder te vinden.

<!--more-->

Deze derde bijeenkomst (lees ook onze artikelen over [bijeenkomst 1](https://klopvaartaardgasvrij.nl/2020-01-13_rekenen_tekenen_sessie1/) en [bijeenkomst 2](https://klopvaartaardgasvrij.nl/2020-02-11_rekenen_tekenen_sessie2/)) bestond uit 2 delen: 
* Overzicht van de berekeningen van de kosten voor eindgebruikers ([presentatie](../files/Eindgebruikerskosten_koplopersbijeenkomst.pdf))
* Communicatie naar de wijk en gebruik van het zogenaamde beoordelingskader ([presentatie](../files/200407_Overvecht-Noord-Uitwerking_Beoordelingskader.pdf))

Voor ons als bewoners van de Klopvaartbuurt is sheet 7 van de ‘Eindgebruikerskosten’ presentatie het meest interessant. 

Een paar opmerkingen bij de berekeningen van CE Delft: 
* Er staat ‘concept’ bij. Dit zijn uitkomsten van voorlopige berekeningen, dit geeft hooguit een idee van welke kant het op gaat. 
* Het MT-net (middentemperatuurwarmtenet) en HT-net ([hogetemperatuurwarmtenet](https://www.ce.nl/assets/upload/image/Plaatjes%20themapagina's/Duurzame%20stad/CE_Delft_5R19_Factsheet_warmtenet_HT_Def.pdf)) komen er wat eindgebruikerskosten betreft, het beste vanaf als het gaat om de aardgasvrije oplossingen die doorgerekend zijn. De HR-ketel (de CV-ketel die u nu heeft) is duidelijk nog steeds goedkoper. Deze uitkomst kan betekenen dat het programmateam gaat sturen in de richting van een warmtenet voor onze buurt. Door de hoge temperatuur van het warmtenet is het niet nodig om de woning ingrijpend te isoleren. Maar door goede isolatie neemt je verbruik natuurlijk nog wel af. 
* Er staan nog steeds forse bedragen bij investeringen en bij doorlopende kosten. Deze kosten zijn voorlopige schattingen. Er bestaat nog veel onduidelijkheid over de prijs van ‘warmte’ en over welke subsidies de gemeente gaat toekennen. Er kan nog veel veranderen dus en dit zegt nog te weinig over onze voorwaarde van ‘betaalbaarheid’. 

De verwachting is dat ergens in juni een compleet rapport van CE Delft beschikbaar komt. In een apart artikel op de website informeren wij u over de manier waarop de werkgroep Klopvaartbuurt Aardgasvrij de wijkbewoners zal informeren en raadplegen. 

Een ander punt heeft te maken met het ‘beoordelingskader’: hoe worden de verschillende oplossingen met elkaar vergeleken? In sheet 4 van de presentatie over het beoordelingskader staat dat CE Delft de oplossingen niet kan vergelijken op het gebied van CO2-uitstoot. Als buurtinitiatieven vinden wij dit merkwaardig. CE Delft heeft al heel veel berekeningen over CO2-uitstoot gedaan en kan nu niet zinvolle informatie geven over de milieu(on)vriendelijkheid van de verschillende warmteoplossingen. In een reactie naar het Programmateam Overvecht-Noord Aardgasvrij en CE Delft hebben wij als buurtinitiatieven aangedrongen op een eerlijke vergelijking van de milieubelasting van de verschillende oplossingen. Daar hoort CO2-uitstoot bij. Zonder deze informatie kunnen we de oplossingen niet goed vergelijken op het gebied van duurzaamheid.