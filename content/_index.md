*Uiterlijk 2030 wil de gemeente Utrecht Overvecht-Noord van het aardgas afhalen, ook de Klopvaartbuurt.*

*Als huiseigenaren in deze buurt willen we invloed hebben op hoe onze buurt van het gas af gaat. Daarom hebben we ons georganiseerd in de Werkgroep Klopvaartbuurt Aardgasvrij.*

*We willen een vertegenwoordiging zijn van de huiseigenaren in de Klopvaartbuurt. Wij bepalen graag zelf wanneer, hoe en onder welke voorwaarden wij van het aardgas af gaan.*

*Woont u in de Klopvaartbuurt en bent u eigenaar van uw huis, meldt u dan aan bij [klopvaart.aardgas@gmail.com](mailto:klopvaart.aardgas@gmail.com) en houdt de website in de gaten.*