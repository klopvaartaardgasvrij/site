---
title: "Brief van de gemeente over Overvecht-Noord Aardgasvrij"
date: 2020-11-25T15:51:38+02:00
url: "/brief-van-de-gemeente-nov-2020"
image: "img/klopvaart7.jpeg"
draft: false
description: "Zoals aangekondigd heeft de gemeente een informatiecampagne gestart over Overvecht-Noord Aardgasvrij. U heeft waarschijnlijk een brief van de gemeente ontvangen. Zoals ook wel verwacht, geeft de gemeente in deze campagne een wat simpel beeld van wat er gaat gebeuren"

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Beste buren,

Zoals aangekondigd heeft de gemeente een informatiecampagne gestart over Overvecht-Noord Aardgasvrij. U heeft waarschijnlijk een brief van de gemeente ontvangen. Zoals ook wel verwacht, geeft de gemeente in deze campagne een wat simpel beeld van wat er gaat gebeuren. 

<!--more-->

Op een aantal plaatsen staat dat ‘we een warmtenet krijgen’. Op andere plaatsen wordt gesproken over een warmtepomp. De situatie is echter zoals aangegeven in onze buurtsessies: we gaan onderzoeken of een buurtwarmtenet een gasloze oplossing kan zijn voor onze buurt. Maar voor ons staan uitdrukkelijk alle opties nog open. En zo hebben we het ook besproken met de gemeente. Er is niet besloten dat wij een warmtenet krijgen. Wij gaan door met het onderzoek naar een buurtwarmtenet. Alleen een oplossing die voldoet aan onze voorwaarden is acceptabel. 

Inmiddels staat er [een uitgebreid verslag van de gesprekken](https://klopvaartaardgasvrij.nl/buurtgesprekken-okt-nov-2020/) die wij met de buurt hebben gehad op onze website.
