---
title: "Welkom nieuwe bewoners!"
date: 2020-01-10T11:50:38+02:00
url: "/buurtinitiatieven-trekken-samen-op"
image: "img/welkom-nieuwe-bewoners.gif"
draft: false
description: "Welkom nieuwe bewoners"
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Welkomstbrief
---

Gefeliciteerd met uw nieuwe woning en welkom in onze wijk De Klopvaartbuurt. In [deze welkomstbrief](https://klopvaartaardgasvrij.nl/files/welkom-nieuwe-bewoners.pdf) kunt u lezen over onze Werkgroep Klopvaartbuurt aardgasvrij. We horen graag uw mening!

<!--more-->