---
title: "Warmteoplossingen"
date: 2019-08-03T10:50:38+02:00
url: "/warmteoplossingen"
image: "img/warmteoplossingen.jpg"
draft: false
description: "Warmteoplossingen"
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Praktisch
---

Met de hele buurt van het gas af, hoe gaan we dat doen?
<!--more-->

Zodra je over 'van het gas af' begint, komt iedereen met allerlei oplossingen aan. Van warmtepomp tot waterstofgas, er zijn heel veel oplossingen mogelijk. De vraag is natuurlijk wel of deze oplossingen 'haalbaar en betaalbaar' zijn, realistisch voor onze buurt en voldoen aan de andere voorwaarden die wij als buurt aan een oplossing willen stellen. De Nederlandse Vereniging  Duurzame Energie heeft een mooi overzichtje gemaakt met voorbeeldprojecten. Dit overzicht is te vinden in [deze PDF](../files/brochure-warmtebronnen-RES3.pdf)

>