---
title: "Buurtbijeenkomsten ‘Warmtepomp voor iedereen’ groot succes"
date: 2023-09-18T16:51:38+02:00
url: "/buurtbijeenkomsten-warmtepomp-voor-iedereen-succes"
image: "img/gesprek.png"
draft: false
description: "In juni 2023 hebben de kernteams van Nieuwe Energie voor de Vechtzoom en Klopvaartbuurt Aardgasvrij in totaal vijf bijeenkomsten georganiseerd om de buurten te informeren over het plan ‘Warmtepomp voor iedereen’. Iedereen die de bijeenkomst heeft bezocht, heeft een enquête ingevuld met vragen over hoe de bewoner aankijkt tegen stadsverwarming en ons. De toegezegde uitwerking van ons plan en onderbouwing van alle claims rond financiering en duurzaamheid en de conclusies zijn te lezen in dit artikel"

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

In juni 2023 hebben de kernteams van Nieuwe Energie voor de Vechtzoom en Klopvaartbuurt Aardgasvrij in totaal vijf bijeenkomsten georganiseerd om de buurten te informeren over het plan ‘Warmtepomp voor iedereen’. Iedereen die de bijeenkomst heeft bezocht, heeft een enquête ingevuld met vragen over hoe de bewoner aankijkt tegen stadsverwarming en ons plan. Het volledige verslag van de enquête en bijeenkomsten vind u [hier](../files/230726_Warmtepomp_voor_Iedereen_-_rapportage_buurtgesprekken_v4.pdf). De toegezegde uitwerking van ons plan en onderbouwing van alle claims rond financiering en duurzaamheid vind u [hier](../files/230419_Warmtepomp_voor_Iedereen_-_onderbouwing_v0.4.pdf).

<!--more-->

Uit de enquête en de bijeenkomsten kunnen we het volgende concluderen:

* Ongeveer een kwart (94) van de huiseigenaren uit onze buurten heeft de enquête ingevuld. Toch is dit niet representatief voor de gehele buurt, want de invullers zijn vooral die bewoners die betrokken zijn bij de plannen voor aardgasvrij en daarover willen meedenken. Om zeker te zijn van een voldoende draagvlak voor het plan ‘Warmtepomp voor iedereen’ zullen we ook de overige bewoners moeten bereiken en hun mening vragen. Dat kan eigenlijk alleen met huiskamergesprekken of huis-aan-huisbezoeken.
* Opvallend is het grote enthousiasme voor het plan ‘Warmtepomp voor iedereen’. 95% van de invullers in onze buurt denkt dat de warmtepomp misschien of zeker het beste alternatief is. Ongeveer de helft weet dat zelfs al zeker.
* Slechts 20% van de invullers heeft een voorkeur voor de stadsverwarming. Ook als ‘Warmtepomp voor iedereen’ niet doorgaat, zullen veel bewoners op zoek gaan naar een alternatief voor stadsverwarming.
* Goede ondersteuning voor de betaalbaarheid van ‘Warmtepomp voor iedereen’ vinden de bewoners belangrijk. De optie om een pomp te huren lijkt nog op weinig belangstelling te kunnen rekenen.
* De stapsgewijze route (eerst hybride en dan later helemaal van het gas af) is niet populair, de grote meerderheid kiest liever voor de voorgestelde PVT-warmtepomp optie, waarmee men volledig van het gas af kan.
De enquête leverde veel vragen op. Op dit moment zijn we bezig om deze vragen één voor één te beantwoorden. Later zullen we dit vraag-en-antwoorddocument ook publiceren.
