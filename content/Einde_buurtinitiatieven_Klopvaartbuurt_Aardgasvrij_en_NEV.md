---
title: "Einde buurtinitiatieven Klopvaartbuurt Aardgasvrij en Nieuwe Energie voor de Vechtzoom"
date: 2024-03-11T16:51:38+02:00
url: "/Einde_buurtinitiatieven_Klopvaartbuurt_Aardgasvrij_en_NEV"
image: "img/klopvaart6.jpeg"
draft: false
description: "De buurtinitiatieven Klopvaartbuurt Aardgasvrij en Nieuwe Energie voor de Vechtzoom worden beëindigd na het verwerpen van twee moties door de gemeenteraad, die het plan 'Warmtepomp voor iedereen' geen ruimte gaven binnen Overvecht-Noord aardgasvrij. Dit besluit volgt op meer dan vijf jaar inzet van de kernteams om een duurzame oplossing voor de buurt te vinden, maar zonder steun van de gemeente voor financiering is het initiatief niet haalbaar. Door de keuze van de gemeente voor stadsverwarming als enige optie, is er geen plaats meer voor een gezamenlijk alternatief, wat leidt tot de opheffing van de buurtinitiatieven. Desondanks blijven de betrokken bewoners informatie over verduurzamingsacties delen en moedigen zij bewoners aan die zelfstandig willen verduurzamen door middel van het plan 'Warmtepomp voor iedereen'."

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

In eerdere berichten op de website hebben wij u geïnformeerd over onze pogingen [richting gemeente en gemeenteraad](https://klopvaartaardgasvrij.nl/brief_gemeenteraad_warmtepomp_voor_iedereen/) om steun te krijgen voor ons plan ‘Warmtepomp voor iedereen’. 21 december 2023 was er een commissiedebat waarin ons plan is besproken. Naar aanleiding van dit debat heeft Eén Utrecht op 29 februari twee moties in stemming gebracht met het dringende verzoek om ons plan alsnog de ruimte te geven binnen Overvecht-Noord aardgasvrij. Beide moties zijn echter verworpen. Daarmee is ons plan als gezamenlijke aanpak voor onze buurt definitief van de baan. Vooral omdat steun van de gemeente noodzakelijk is om een deel van onze buren te helpen met de financiering. Meer dan vijf jaar lang hebben de kernteams zich ingezet om met een goede oplossing voor onze buurt te komen. Dit resultaat is dan ook zwaar teleurstellend.


<!--more-->

4 maart 2024 hebben de kernteams van Klopvaartbuurt Aardgasvrij en Nieuwe Energie voor de Vechtzoom zich gebogen over de vraag wat dit betekent voor het voortbestaan van onze buurtinitiatieven. Omdat de gemeenteraad nu duidelijk heeft gemaakt dat stadsverwarming de enige optie wordt voor wijken die van het gas af moeten, is er dus geen ruimte meer voor een gezamenlijk alternatief. Om die reden beëindigen we onze buurtinitiatieven: we zijn niet meer het aanspreekpunt voor de gemeente voor het aardgasvrij maken van onze buurt.

Als betrokken bewoners houden wij u nog wel graag op de hoogte van gezamenlijke verduurzamingsacties, zoals een nieuwe isolatie-actie, waar we mee bezig zijn. Daarom houden wij de website en de mailinglist nog in de lucht. Ook omdat op onze website een schat aan informatie te vinden is over [ons plan ‘Warmtepomp voor iedereen’](https://klopvaartaardgasvrij.nl/brief_gemeenteraad_warmtepomp_voor_iedereen/).

Bovendien biedt ons plan de mogelijkheid om nu al van het gas af te gaan en daarmee nu al te verduurzamen én geld te besparen. Mocht u ook op korte termijn willen starten en heeft u budget of bereidheid om te lenen, dan kunt u aansluiten bij een (nu nog klein) groepje koplopers. Contacten met diverse leveranciers zijn al gelegd. Wij komen zo snel mogelijk met informatie over hoe u kunt meedoen. 
