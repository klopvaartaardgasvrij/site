---
title: "Samen isoleren"
date: 2021-11-16T07:51:38+02:00
url: "/samen-isoleren-nov-2021"
image: "img/isolatie.png"
draft: false
description: "Afgelopen jaar is er veel gebeurd in onze buurt. We hebben, ondanks de pandemie, niet stil gezeten met het aardgasvrij worden van onze wijk. Omdat we niet enkel willen kijken naar de aanbodkant van de warmte, maar ook naar de vraagkant zijn we bezig met het samen isoleren van onze huizen en onze mooie wijk."

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Afgelopen jaar is er veel gebeurd in onze buurt. We hebben, ondanks de pandemie, niet stil gezeten met het aardgasvrij worden van onze wijk. Omdat we niet enkel willen kijken naar de aanbodkant van de warmte, maar ook naar de vraagkant zijn we bezig met het samen isoleren van onze huizen en onze mooie wijk.

<!--more-->

Gezamenlijk met de gemeente hebben we een onderzoek uitgevoerd om te kijken welke isolerende maatregelen nu een direct effect kunnen hebben op de warmtevraag en het comfort in de woning. Hier zijn drie maatregelen uitgekomen die we stukje voor stukje aanbieden aan de bewoners, jullie dus.

Van boven naar beneden hebben we als eerste de dakisolatie. Als je van plan bent om een nieuw dak te plaatsen, is het aan te raden om direct extra isolatiemateriaal aan te brengen. Omdat dit een hele dure post is als je je dak niet per se vervangen wilt, is er ook de mogelijkheid om er isolerende parels in aan te brengen. Het bedrijf dat ons hierin helpt klimt het dak op en haalt een aantal dakpannen tijdelijk weg. Met behulp van een slang wordt er dan een laag isolerende verlijmde parels onder de dakpannen gespoten. De zonnepanelen die er eventueel liggen, moeten er wel van te voren afgehaald worden.

Vervolgens zijn de spouwmuren aan de beurt. Ook hier wordt een extra isolatielaag aangebracht middels de verlijmde parels. Dit werkt heel erg makkelijk. Er moeten een paar kleine gaatjes in de muur geboord worden en dan kunnen de parels weer middels een slang ingespoten worden. Het is belangrijk om bij het aanvragen van de offerte ook gelijk even te laten kijken of de vochtafvoer bij de muur goed is. Mocht er vocht in de muur zitten, dan verwarm je via je isolatielaag de buitenlucht en dat kan nooit een goed idee zijn. Het bedrijf dat ons hierin assisteert controleert dat ook bij het opstellen van de offerte.
En dan zijn we alweer aanbeland bij de bodem. Ook hier kunnen in de kruipruimte parels ingespoten worden. Tevens zijn er zakken verkrijgbaar die tegen de wanden in de kruipruimte gelegd kunnen worden, zodat de warmte vanuit de kruipruimte naar boven gaat en niet naar buiten.

We hebben net ronde 1 van het isoleren gehad en we zijn heel erg benieuwd naar de meningen van de huiseigenaren die voorop liepen in het isoleren. Hoe comfortabel is de woning nu ten opzichte van een winter eerder en hebben ze het idee dat er minder warmte gevraagd wordt voor het op temperatuur brengen en houden van de woning? In het voorjaar komen we terug om de resultaten te presenteren en te kijken of er weer een aantal huiseigenaren zijn die graag mee willen doen in ronde 2.
Mocht het allemaal bevallen en goed gaan, dan blijven we gewoon elk jaar weer een isolatieronde aanbieden. Wil je op de hoogte blijven? Stuur dan even een e-mail naar klopvaart.aardgas@gmail.com
