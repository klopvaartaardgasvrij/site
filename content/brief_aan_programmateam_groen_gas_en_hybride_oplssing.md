---
title: "Brief aan Programmateam over groen gas en over hybride oplossing"
date: 2020-05-16T21:10:38+02:00
url: "/brief_aan_programmateam_groen_gas_en_hybride_oplssing"
image: "img/warmteoplossingen.jpg"
draft: false
description: "Brief aan Programmateam over groen gas en over hybride oplossing"
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Voor Overvecht-Noord Aardgasvrij rekent het bureau CE Delft diverse oplossingen door om van het gas af te gaan. Tijdens het rekenproces heeft CE Delft aangegeven dat een aantal oplossingen niet relevant zouden zijn voor Overvecht-Noord. Hierover hebben wij een brief opgesteld.

<!--more-->

Als buurtinitiatieven (Nieuwe Energie Vechtzoom, Klopvaartbuurt Aardgasvrij) vonden wij de argumentatie van CE Delft om ‘groen gas’ niet mee te nemen, onduidelijk. Ook vonden wij dat de optie van een hybride oplossing serieus onderzocht zou moeten worden, omdat de situatie in Overvecht-Noord is veranderd en ook de [grote energiebedrijven](https://www.energie-nederland.nl/laagdrempelige-energietransitie/) en [Stedin](https://stedin.net/over-stedin/~/media/files/stedin/position-papers/waterstof-workingpaper.pdf?la=nl-nl) op dit moment voor een hybride oplossing zijn. 

Een hybride oplossing is een kleine warmtepomp zonder buitenunit die je samen met je CV-ketel inzet om je huis te verwarmen. De warmtepomp doet het grote werk, de CV-ketel springt bij als dat nodig is. Geen ingrijpende verbouwing of extreme isolatie nodig. En fors minder gasverbruik. De gasleidingen blijven liggen en kunnen misschien later voor groen gas gebruikt worden. 

De buurtinitiatieven hebben [een brief (lees de brief hier)](../files/20200413_Brief_buurtinitiatieven_groen_gas_en_hybride_oplossing.pdf) opgestuurd naar het Programmateam Overvecht-Noord Aardgasvrij om aandacht te vragen voor groen gas en voor hybride oplossingen. Het Programmateam heeft aangegeven met CE Delft in gesprek te gaan om te onderzoeken hoe deze oplossingen in het selectieproces een plaats kunnen krijgen. 

