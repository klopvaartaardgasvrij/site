---
title: "Tweede werksessie 'rekenen en tekenen' aan alternatieven voor aardgas"
date: 2020-03-10T20:50:38+02:00
url: "/2020-02-11_rekenen_tekenen_sessie2"
image: "img/200211_buurtindeling_CBS_woningtype.jpg"
draft: false
description: "werksessie 2"
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Na de [eerste werksessie](https://klopvaartaardgasvrij.nl/2020-01-13_rekenen_tekenen_sessie1/) vond op 11 februari de tweede werksessie plaats. In deze sessie kwamen de doorgerekende kosten van de nationale kosten voor verschillende alternatieven voor aardgasvrij aan de orde. De presentatie van deze avond vind u [hier](../files/200211_2eWerksessie_rekenenentekenen_APPM.pdf).

<!--more-->

De avond riep veel vragen op, die verder besproken worden met APPM en CE Delft.

Lees ook [het verslag van de derde reken- en tekensessie](https://klopvaartaardgasvrij.nl/2020-04-14_rekenen_tekenen_sessie3/) die op 14 april gehouden is.