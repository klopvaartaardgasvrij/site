---
title: "Buurtinitiatieven Overvecht Noord trekken samen op"
date: 2020-01-25T11:51:38+02:00
url: "/buurtinitiatieven-trekken-samen-op-"
image: "img/klopvaart.jpg"
draft: false
description: "buurtinitiatieven trekken samen op"
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Beste buren,

Afgelopen 22 januari heeft de Stuurgroep Overvecht Noord Aardgasvrij voor het eerst vergaderd. Het is een stuurgroep die door de gemeente Utrecht is opgericht om de warmtetransitie in oa onze buurt goed te laten verlopen. Het eerste agendapunt? Een [brief](https://klopvaartaardgasvrij.gitlab.io/site/files/Brief_partners_programma_organisatie_def.pdf) ingezonden door de twee buurtinitiatieven van Overvecht Noord: Klopvaartbuurt Aardgasvrij en Nieuwe Energie Vechtzoom.

<!--more-->

Als werkgroep Klopvaartbuurt Aardgasvrij hebben we gezien dat er naast ons nog een buurtinitiatief actief is, [Nieuwe Energie Vechtzoom ](http://www.energievechtzoom.nl) (omgeving Costa Ricadreef). Omdat onze belangen zeer gelijk zijn en je samen sterker staat dan alleen hebben we besloten dat we veel samen op willen trekken naar de gemeente toe. We blijven elk een eigen buurtinitiatief, maar zullen wel veel overleg hebben en veel samen delen.

Nieuwe Energie Vechtzoom heeft een manifest opgesteld met daarin een aantal voorwaarden die bijna gelijk zijn aan de voorwaarden waar we samen voor getekend hebben. Er zijn echter ook verschillen en dat is mede de reden dat we samen optrekken, maar wel twee eigen buurtinitiatieven zijn. Zo zijn er duidelijke verschillen in de buurt wat betreft soort huizen. Dit kan van betekenis zijn voor de alternatieven waar de gemeente mee gaat komen. Een goed alternatief voor het ene huis, of voor de ene buurt, is misschien niet het beste alternatief voor de ander. Omdat we nog geen alternatieven gehoord hebben, is het slimmer om naast elkaar te bestaan in plaats van in elkaar op te gaan.

De buurten zijn ook verschillend qua grootte en dit kan ook een effect hebben op de gekozen warmteoplossingen. Waar beide buurten echter geheel gelijkgezind zijn is dat we graag mee worden genomen in de besluitvorming en dat we, als huiseigenaren, zeggenschap hebben en blijven houden over ons eigen huis. Dus ook over de manier waarop dit verwarmd wordt. Vandaar dat we samen een [brief](https://klopvaartaardgasvrij.gitlab.io/site/files/Brief_partners_programma_organisatie_def.pdf) opgesteld en verstuurd hebben naar de stuurgroep van alle samenwerkende partners. In deze brief uiten wij onze wens om, samen met de gemeente en de partners, een goede en mooie samenwerking te hebben met als eindresultaat een wijk waarin alle huizen op een goede, comfortabele, duurzame en betaalbare manier lekker warm gestookt kunnen worden, zonder dat je in de winter een continu gebrom van warmtepompen hoort. De stuurgroep was positief verrast door beide buurtinitiatieven en heeft aangegeven om samen met ons het proces aan te gaan.

In de tussentijd hebben we niet stil gezeten en zijn er al meerdere bijeenkomsten geweest. Bij elke bijeenkomst proberen we een afvaardiging van beide buurtinitiatieven aanwezig te hebben, we bespreken ook vooraf met elkaar waar we op willen letten en wat voor elke buurt van belang is. Het is een fijne samenwerking waar we zeker vruchten van kunnen plukken.
 
