---
title: "Huis-aan-huis actie is groot succes"
date: 2020-01-19T13:50:38+02:00
url: "/huis-aan-huis-groot-succes"
image: "img/klopvaartbuurt_from_above_3d.jpg"
draft: false
description: "burenactie"
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Met hulp van een aantal vrijwilligers zijn we huis aan huis de deuren langs gegaan om steun te vragen voor [ons buurtinitiatief Klopvaartbuurt Aardgasvrij](https://klopvaartaardgasvrij.gitlab.io/site/burenactie/). Over het algemeen hebben bewoners veel waardering voor ons initiatief om als buurt samen op te trekken. Ook de persoonlijke benadering werd zeer gewaardeerd, er zijn veel goede gesprekken gevoerd over de plannen om onze buurt van het gas af te halen. 

Klik hieronder om meer te lezen.

<!--more-->

De resultaten in cijfers:

* 63% van de bewoners heeft gereageerd: van de ongeveer 280 grondgebonden koopwoningen hebben we van 178 adressen het formulier teruggekregen.
* Van de 178 formulieren die we hebben ontvangen, zijn er 3 geweest die Nee hebben aangekruist. Een overweldigende meerderheid steunt ons initiatief, bedankt daarvoor!
* Ongeveer 280 bewoners hebben hun handtekening op het formulier gezet.

Er zijn veel opmerkingen gemaakt op de antwoordformulieren en vragen gesteld (40). De komende tijd gaan we deze vragen beantwoorden. Eén van de opmerkingen die we meerdere keren terug kregen was dat men bang is voor geluidsoverlast van warmtepompen. We nemen dit mee als aanvullende voorwaarde: de warmteoplossing mag niet leiden tot extra geluidsoverlast. 

Inmiddels hebben we de gemeente officieel op de hoogte gesteld van ons buurtinitiatief. 

In de brief waarmee we langs de deuren zijn geweest hebben we voorwaarden genoemd waaronder we eventueel wel van het gas af willen gaan. Deze voorwaarden zijn inmiddels door de gemeente opgepikt in hun zoektocht naar oplossingen. 

## Reageren?

Wilt u reageren of heeft u vragen? Stel ze via <a href="mailto:klopvaart.aardgas@gmail.com">klopvaart.aardgas@gmail.com</a>