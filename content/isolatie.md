---
title: "Isolatie"
date: 2019-08-02T13:50:38+02:00
url: "/isolatie"
image: "img/isolatie.png"
draft: false
description: "Isolatie"
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Praktisch
---

In september komt er een nieuwe subsidiepot voor isolatie.
<!--more-->

Het rijk komt in september met een nieuwe subsidiepot voor isolatie. Het zou mooi zijn als we hier als Klopvaartbuurt wat van meepakken. Tijdens de gesprekken in onze buurt bleek dat er veel belangstelling is voor isolatie, maar dat kosten een probleem kunnen zijn. Volgens enkele kenners uit onze buurt zouden we moeten beginnen met kierdichting en vloerisolatie. Graag zouden we als buurt hierin samen optrekken om met gezamenlijke inkoop de prijzen te drukken. We houden jullie op de hoogte. Kijk [hier](https://www.hier.nu/themas/investeren-in-je-huis/waar-moet-je-op-letten-als-je-een-huis-gaat-kopen) voor wat ideeën voor isolatie. 