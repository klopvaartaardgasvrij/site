---
title: "Planning voor de bespreking van de aardgasvrije opties van de gemeente"
date: 2020-05-27T20:38:38+02:00
url: "/planning_aardgasvrije_opties"
image: "img/stappenplan.png"
draft: false
description: "Planning aardgasvrije opties met gemeente"
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Om ervoor te zorgen dat de geboden oplossingen van de gemeente aan onze voorwaarden voldoen hebben we samen met het buurtinitiatief Nieuwe Energie Vechtzoom (NEV) een stappenplan opgezet.

<!--more-->

Binnen het kernteam wordt er hard gewerkt aan het scherp houden van de gemeente. Over 10 jaar moeten we in Overvecht Noord van het gas af zijn. Dat klinkt nog best ver, maar tevens is het ook best dichtbij! De gemeente heeft het onafhankelijke advies- en onderzoeksbureau CE Delft de opdracht gegeven om de verschillende mogelijkheden voor een aardgasvrij Overvecht Noord door te rekenen. Het moet haalbaar en betaalbaar zijn. Dat heeft onze wethouder, mevrouw van Hooijdonk, beloofd. Maar behalve haalbaar en betaalbaar hebben we met onze buurt [een aantal voorwaarden opgesteld](https://klopvaartaardgasvrij.nl/burenactie/) waaraan deze oplossing(en) moeten voldoen. Hoe zorgen we er nu voor dat de geboden oplossingen aan die voorwaarden voldoen? Om dat goed in kaart te brengen hebben we, samen met het [buurtinitiatief Nieuwe Energie Vechtzoom (NEV)](http://www.energievechtzoom.nl/), het volgende stappenplan opgezet.

Eind mei, begin juni is het rapport van CE Delft over de haalbaarheid van de verschillende oplossingen gereed. Dit rapport krijgen wij ook om zo onze mening te vormen. Veel nieuws kan er niet instaan, want bij elke reken- en tekensessie waren we vertegenwoordigd. Omdat er na elke sessie toch best wat vragen en opmerkingen zijn is het wel van belang om te zien wat ze daarmee gedaan hebben en of dit voor ons voldoende is. Om dit goed te bekijken hebben we met ons kernteam een avond in juni/juli besproken om dit rapport goed te bespreken en een duidelijke mening te vormen van wat wij er als buurtinitiatief van vinden. Dit is het moment waarop we onze voorwaarden erbij pakken en elke geboden oplossing naast onze voorwaarden leggen om te zien of de oplossing voldoet of niet.

In juli/augustus gaan we samen met NEV zitten om te zien wat zij van het rapport vinden en om te zien in hoeverre dat overeenkomt met wat wij er van vinden. In het manifest van NEV staan ongeveer gelijke voorwaarden als de voorwaarden waar wij met z’n allen voor getekend hebben. Ook zij hebben met hun buurtinitiatief bij elkaar gezeten om het rapport uit te pluizen.

De punten die hier uit gaan komen waarvan wij vinden dat ze niet (geheel) voldoen aan onze voorwaarden gaan we bundelen en bieden we vervolgens aan aan het programmateam van de gemeente. Als de gemeente wil dat wij jullie, bewoners van de Klopvaartbuurt, positief adviseren, dan zullen deze punten verbeterd moeten worden. In september/oktober zullen we met jullie in gesprek gaan over de geboden oplossingen voor onze buurt.  Nadat de gemeente de opties met bewoners heeft besproken, zal de meest kansrijke optie verder uitgewerkt gaan worden. Dat betekent ook dat wij als bewoners nog nergens aan vast zitten, maar dat er wel opties gaan afvallen. Als kernteam vinden wij het zeer belangrijk met jullie goed af te stemmen wat wij hiervan vinden.

Belangrijk om te weten is dat wij niet van plan zijn om de communicatie van de gemeente over te nemen, of dat wij in op dracht van de gemeente zouden werken. In tegendeel! Wij hebben onszelf opgericht omdat we de gemeente willen toetsen aan hun eigen én onze voorwaarden om van het gas af te gaan.

Houd tot die tijd vooral ook de website en de mailing in de gaten. We zijn hard aan het werk om onze mooie Klopvaartbuurt mooi en fijn te houden.

Kernteam Klopvaartbuurt Aardgasvrij
