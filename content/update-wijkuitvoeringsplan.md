---
title: "Update: wijkuitvoeringsplan"
date: 2022-09-11T07:51:38+02:00
url: "/update-wijkuitvoeringsplan"
image: "img/klopvaart3.jpeg"
draft: false
description: "Gebeurt er nog wat met 'aardgasvrij'? Ja. De gemeente werkt aan een WijkUitvoeringsPlan (WUP) waarin staat hoe heel Overvecht-Noord op de stadsverwarming zal gaan. Dus ook onze buurt. In dit WUP staat de planning en ook de kosten en wie dat gaat betalen. De gemeente is nu druk in gesprek met Eneco om er financieel uit te komen. Begin 2023 komt er een inspraakronde over het concept van het WUP. Daarna gaat het naar de gemeenteraad die er zijn goedkeuring aan moet geven. Vanzelfsprekend zal het kernteam de plannen uit het WUP naast onze 'voorwaarden' leggen en naar jullie terugkoppelen wat wij hier van vinden."

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Gebeurt er nog wat met 'aardgasvrij'? Ja. De gemeente werkt aan een WijkUitvoeringsPlan (WUP) waarin staat hoe heel Overvecht-Noord op de stadsverwarming zal gaan. Dus ook onze buurt. In dit WUP staat de planning en ook de kosten en wie dat gaat betalen. De gemeente is nu druk in gesprek met Eneco om er financieel uit te komen. Begin 2023 komt er een inspraakronde over het concept van het WUP. Daarna gaat het naar de gemeenteraad die er zijn goedkeuring aan moet geven. Vanzelfsprekend zal het kernteam de plannen uit het WUP naast onze 'voorwaarden' leggen en naar jullie terugkoppelen wat wij hier van vinden. 
