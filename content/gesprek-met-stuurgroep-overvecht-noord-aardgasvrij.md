---
title: "Gesprek buurtinitiatieven en Stuurgroep Overvecht-Noord Aardgasvrij"
date: 2020-03-27T20:51:38+02:00
url: "/gesprek-met-stuurgroep-overvecht-noord-aardgasvrij"
image: "img/gesprek.png"
draft: false
description: "Begin maart spraken de beide kernteams van de buurtinitiatieven Klopvaartbuurt Aardgasvrij en [Nieuwe Energie Vechtzoom (NEV) met de net-opgerichte stuurgroep Overvecht-Noord Aardgasvrij. Vanuit de stuurgroep waren vertegenwoordigd de gemeente (wethouder Lot van Hooijdonk), Stedin, Energie-U en Mark Elbers."

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Begin maart spraken de beide kernteams van de buurtinitiatieven (Klopvaartbuurt Aardgasvrij en [Nieuwe Energie Vechtzoom (NEV)](http://www.energievechtzoom.nl) met de net-opgerichte stuurgroep Overvecht-Noord Aardgasvrij.

<!--more-->

Vanuit de stuurgroep waren vertegenwoordigd de gemeente (wethouder Lot van Hooijdonk), Stedin, Energie-U en Mark Elbers. Dit gesprek was het vervolg op een brief die wij samen met NEV in januari hebben geschreven. De vraag was: Hoe maken we samen een situatie waarin wij als buurten goed kunnen meepraten over de oplossingen  en hoe zorgen we dat zoveel mogelijk bewoners daarbij betrokken zijn?

Gemeente en partners zullen komende jaren voor bewoners informatie, bijeenkomsten en raadplegingen gaan organiseren. Wij willen zorgen dat dit goed aansluit en goed landt in onze buurt. Dat er geen dingen dubbel gaan gebeuren of langs elkaar heen werken. En dat
we bewoners betrekken waar de gemeente dat veel minder goed kan. Dat vraagt van gemeente en partners dat ze ons als gelijkwaardige partner zien en dat we samen afspraken maken over hoe dat in onze buurt georganiseerd gaat worden. Voor beide kanten is het best spannend. Wij als buurtinitiatief moeten vrij blijven om op de rem te trappen als we merken dat de plannen niet voldoen aan de voorwaarden die zijn afgesproken met de buurt of niet op draagvlak kunnen rekenen. Dit eerste kennismakingsgesprek verliep in een goede positieve sfeer. Komende maanden komen er een aantal vervolggesprekken om te kijken hoe we de samenwerking kunnen invullen.

(Met dank aan de nieuwsbrief van NEV)
