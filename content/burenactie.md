---
title: "Burenactie"
date: 2019-09-22T13:50:38+02:00
url: "/burenactie"
image: "img/klopvaart.jpg"
draft: false
description: "burenactie"
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Wij hebben brieven verspreid in de Klopvaartbuurt en zullen de deuren langsgaan om met onze buren in gesprek te gaan. Zo willen we te weten komen hoe de huiseigenaren in de Klopvaartbuurt denken over aardgasvrij. Uit een eerdere enquête bleek dat we best willen meehelpen met het aardgasvrij maken van Nederland, vooral uit het oogpunt van duurzaamheid. Maar we willen niet een proeftuin zijn die veel moet investeren en hogere maandlasten heeft. Wij nemen nu het initiatief en hebben voorwaarden opgesteld waaronder   wij willen meewerken aan het aardgasvrij maken van Nederland. 

Klik hieronder om meer te lezen.

<!--more-->

Uiterlijk 2030 wil de gemeente Utrecht Overvecht-Noord van het aardgas afhalen. Onze Klopvaartbuurt is onderdeel van Overvecht-Noord en valt dus binnen deze plannen.

Als huiseigenaren willen we wel invloed hebben op welke warmteoplossingen er aangeboden gaan worden en hoe we na 2030 ieder van onze woningen betaalbaar, betrouwbaar en duurzaam kunnen blijven verwarmen. Daarom hebben we ons georganiseerd in de werkgroep Klopvaartbuurt Aardgasvrij. Alles over ons en de informatie die we verzamelen kunt u vinden op een speciale website www.klopvaartaardgasvrij.nl 
De werkgroep wil een vertegenwoordiging zijn van de huiseigenaren in de Klopvaartbuurt. Wij bepalen graag zelf wanneer, hoe en onder welke voorwaarden wij van het aardgas af gaan.

Omdat niemand van ons een expert is in warmteoplossingen hebben we, in navolging van de buurt de Vechtzoom, een aantal voorwaarden opgesteld. Hierin geven wij bij de gemeente aan dat wij als buurt wel mee willen werken aan het aardgasvrij maken van Nederland, maar dat we dit wel willen doen binnen door ons duidelijk gestelde voorwaarden. Voorwaarden waar we als buurt samen JA tegen kunnen zeggen. Deze voorwaarden kunt u vinden in de bijlage bij deze brief. Als er na het lezen van deze brief en bijlage nog vragen zijn kunt u zich altijd melden bij klopvaart.aardgas@gmail.com. Hier kunt u zich ook melden om op de hoogte te blijven van de activiteiten van de werkgroep.
Om als werkgroep ook echt een serieuze gesprekspartner te kunnen zijn bij de gemeente is het van belang dat we een zo groot mogelijk deel van de buurt vertegenwoordigen. Daarom vindt u bij deze brief een invulstrook die we over een week of twee graag komen halen, zodat we kunnen laten zien dat wij er als werkgroep ook voor u zijn. 

Onlangs is er een enquête in de buurt gehouden om te onderzoeken wat de buurt vindt van de transitie naar aardgasvrij wonen. Daaruit blijkt dat men best wil meehelpen met het aardgasvrij maken van Nederland, vooral uit het oogpunt van duurzaamheid. Maar we willen niet een proeftuin zijn die veel moet investeren en hogere maandlasten heeft. Vandaar dat we, in navolging van de buurt de Vechtzoom, de volgende voorwaarden hebben opgesteld om de gemeente te dwingen naar meerdere opties te kijken als warmteoplossing, dan de eerste de beste.

Wat wij willen:

## Duurzaam
De geboden oplossing mag niet meer CO2 produceren dan nu het geval is. Hoe komt een warmtenet aan zijn energie?

## Betaalbaar
Geen onhaalbare investering die betaald moet worden door de huiseigenaren.
De maandelijkse kosten mogen niet hoger zijn dan die van het verwarmen van je huis met gas.

## Onopvallend
Geen grote en lelijke kasten in het zicht of in je huis. Een warmtepomp op zolder kost veel ruimte. Warmtepompen zoals in Woerden zijn groot en lelijk.

## Zeggenschap
De gemeente komt met meerdere opties waar de buurt uit kan kiezen. Er kan per huis, per blok of per buurt gekozen worden voor de beste optie.

## Haalbaar

Niet een oplossing die zo ingewikkeld is dat u als huiseigenaar de aanpassingen aan uw woning niet ziet zitten.

## Bewezen
Geen experimentele nieuwe oplossingen die nog niet bewezen zijn.

## Comfortabel
De geboden warmteoplossing moet hetzelfde comfort en gemak bieden, met dezelfde betrouwbaarheid, als nu. We willen niet in de kou zitten als het een keer echt goed vriest.

# Vragen?

Heeft u nog vragen? Stel ze via <a href="mailto:klopvaart.aardgas@gmail.com">klopvaart.aardgas@gmail.com</a>