---
title: "Eerste werksessie 'rekenen en tekenen' aan alternatieven voor aardgas"
date: 2020-01-31T20:50:38+02:00
url: "/2020-01-13_rekenen_tekenen_sessie1"
image: "img/200113_buurtindeling_CBS.png"
draft: false
description: "werksessie 1"
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Komende maanden gaan de bureaus APPM en CE Delft de alternatieven voor aardgas voor Overvecht-Noord doorrekenen. Zij gaan inventariseren welke technieken in de wijk gebruikt kunnen worden en wat deze kosten. Zij verzamelen data die al beschikbaar is, vullen deze aan met gegevens van de verschillende warmtetechnieken en maken modelberekeningen. Daarvoor organiseren ze een serie technisch georiënteerde werksessies, om te informeren en feedback op te halen. Afgelopen maandag 13 januari 2020 vond de 1e werksessie ‘rekenen en tekenen aan alternatieven voor aardgas’ plaats in de Vechtsebanen. 

Klik hieronder om meer te lezen en de presentatie van de sessie te bekijken.

<!--more-->

De sessie met [APPM](http://www.appm.nl) en [CE Delft](http://www.ce.nl) bestond uit drie hoofdonderdelen:

1. Introductie plan van aanpak
2. Introductie in verschillende warmtetechnieken
3. Introductie berekeningen 

De gepresenteerde slides zijn [hier](../files/200113_PresentatieAPPMCE_Start_OVN_AGV.pdf) te bekijken. Hieronder onze korte samenvatting.

## 1. Introductie plan van aanpak

Het plan van aanpak bestaat uit vier stappen:
1. Voorkeurstechnieken
2. Inloopdagen (2 dagen)
3. Gebruikerskosten
4. Uitvoeringsplan

### Stap 1: voorkeurstechnieken

![Plan van aanpak stap 1](../img/200113_stap1-plan_van_aanpak.jpg "Plan van aanpak stap 1")

In de eerste stap wordt Overvecht-Noord onderverdeeld  in de vier CBS-buurten, zoals te zien is op het kaartje hieronder. In deze verdeling valt de Klopvaartbuurt binnen een bredere buurt, wat het risico met zich meebrengt dat zeer verschillende woningen binnen deze grove sub-buurten op een hoop worden gegooid. Het doel van deze stap is om naar 1 tot 3 oplossingen per sub-buurt te komen. Deze selectie wordt gemaakt op basis van ‘nationale kosten’, niet van de kosten van de eindgebruiker (zie slide 34 waarom ze deze  keuze maken). Deze stap zal plaatsvinden in januari en februari en zal vergezeld worden van twee werksessies.

![CBS sub-buurten](../img/200113_buurtindeling_CBS.png "CBS sub-buurten die gebruikt worden voor stap 1")

### Stap 2: Inloopdagen

Het doel van de tweede stap is om de resultaten van stap 1 te delen met bewoners en om ideeën op te halen. In deze stap worden de concrete gevolgen voor bewoners gecommuniceerd.

### Stap 3: Gebruikerskosten

In de derde stap komen de kosten per woning(type) in beeld. Hier gaat het wel over de eindgebruikerskosten, dus welk deel van de totale kosten bij de individuele huiseigenaren terecht komt. Hierbij worden aannames gemaakt over de huizen; alle veranderingen die huiseigenaren aan hun woning hebben gedaan worden hierin dus niet meegenomen. Dit project levert advies aan de gemeente over de mogelijke oplossingen (lettend op de totale nationale kosten), het is vervolgens aan de gemeente om tot een aanbod te komen per woning, inclusief de opties voor financiering. Deze stap staat voor februari en maart gepland, met een extra werksessie.

### Stap 4: Uitvoeringsplan

De laatste stap moet resulteren in een advies per woning. In deze stap wordt een ‘vlekkenkaart’ van de wijk gemaakt, waarop de adviezen per woning worden gebaseerd. De aanwezige bewoners zijn erg bezorgd dat deze vlekkenkaart er niet uit zal zien zoals wij de indeling van de wijk het meest logisch vinden (zie ook de zorgen bij stap 1). APPM en CE Delft denken dat dit de beste methode is om tot de eindoplossing per gebruiker te komen en nodigt ons bewoners uit om onze input te geven tijdens de werksessies. Dat zullen wij zeker doen! Deze stap staat gepland voor maart / april / mei.

## 2. Introductie in verschillende warmtetechnieken

In dit deel van de presentatie werd Zie voor deze introductie de [slides van de avond](../files/200113_PresentatieAPPMCE_Start_OVN_AGV.pdf) en de [website van CE Delft over de verschillende warmtetechnieken](http://www.warmtetechnieken.nl).

## 3. Introductie berekeningen 

Warmteoptie bestaat uit drie ‘knoppen’ om aan te draaien:
* Warmtevraag: Gebouwschil (isolatie, kieren) (én bewonersgedrag)
* Gebouwinstallatie
* Energiedrager (infra) (groen gas, elektra, warmte, WKO)

Om aardgasvrij te worden in 2030 gaat CE uit van momenteel beschikbare technieken en energiedragers (zie [slides](../files/200113_PresentatieAPPMCE_Start_OVN_AGV.pdf)), waardoor waterstof afvalt.

Op basis van nationale kosten selecteert CE per sub-buurt warmtetechnieken voor een berekening van eindgebruikerskosten. Daarna zoomen ze in op de woningen in de buurten (de eerder genoemde vlekkenkaart), op basis van de huidige tarieven en belastingen. Om te zien of de warmteoptie betaalbaar is voor de bewoners worden kostenberekeningen (in euro’s per jaar) gemaakt op basis van het energielabel per gebouw en marktkosten per warmteoplossing. Deze berekening levert een bedrag op over alle woningen in een cluster (in de vlekkenkaart), daarna worden de kosten verdeeld over de woningen in het cluster. Daarbij wordt de aanname gemaakt dat alle woningen in het cluster meedoen met de aangeboden warmteoplossing. 

Nu gaan APPM en CE Delft deze data gebruiken voor de berekeningen in stap 1:

![Gebruikte gegevens](../img/200113_gebruikte_gegevens.jpg "Gebruikte gegevens")

CE roept ons als laatste op om informatie waarvan wij bewoners vinden dat ze die in de berekeningen moeten meenemen te mailen via <a href="mailto:overvechtnoord@ce.nl">overvechtnoord@ce.nl</a>.

De tweede werksessie staat gepland voor dinsdagavond 11 februari van 19:30 uur tm 21:30 uur. Aanmelden kan t/m 5 februari via <a href="mailto:energie@utrecht.nl">energie@utrecht.nl</a>.

Wij houden u via deze website op de hoogte. Wilt u reageren of heeft u vragen? Stel ze via <a href="mailto:klopvaart.aardgas@gmail.com">klopvaart.aardgas@gmail.com</a>