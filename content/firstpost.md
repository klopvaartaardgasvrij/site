---
title: "Lancering website Klopvaart Aardgasvrij & dia's buurtbijeenkomsten"
date: 2019-07-28T22:50:38+02:00
url: "/lancering_website"
image: "img/klopvaart.jpg"
draft: false
description: "Klopvaart"
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Welkom op de website van Klopvaart Aardgasvrij! Op deze plek houden wij u op de hoogte van de ontwikkelingen rondom de Aardgasvrij-initatieven in de Klopvaartbuurt.

Klik hieronder om de dia's van de buurtbijeenkomsten te bekijken.

<!--more-->

De dia's van de buurtbijeenkomsten kunnen [hier](../files/buurtbijeenkomsten-klopvaart.pdf) gevonden worden.
