---
title: "Vragen van bewoners & antwoorden"
date: 2020-02-15T11:51:38+02:00
url: "/vragen-van-bewoners"
image: "img/vragen.png"
draft: false
description: "De huis-aan-huis actie in de Klopvaartbuurt in Utrecht Overvecht heeft veel vragen opgeleverd. Deze vragen hebben we verzameld in geprobeerd in één keer te beantwoorden. Hier zijn de antwoorden te vinden."
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Onze huis-aan-huis actie heeft veel vragen opgeleverd. Deze vragen hebben we verzameld in geprobeerd in één keer te beantwoorden.

<!--more-->

In [dit bestand](https://klopvaartaardgasvrij.nl/files/klopvaart-aardgasvrij-vragen-van-bewoners-met-antwoorden.pdf) vindt u onze antwoorden.