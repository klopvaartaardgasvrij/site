---
title: "Wel of geen zonnepanelen plaatsen?"
date: 2021-05-15T04:51:38+01:00
url: "/zonnepanelen-nu-of-wachten"
image: "img/Zonnepanelen-s.jpeg"
draft: false
description: "Een aantal keer heeft ons buurtinitiatief de vraag gekregen of je nu al wel zonnepanelen op je dak kunt neerleggen of toch maar beter even kunt wachten totdat duidelijk wordt hoe we van het gas af gaan. Hier behandelen we deze vraag"

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Een aantal keer heeft ons buurtinitiatief de vraag gekregen of je nu al wel zonnepanelen op je dak kunt neerleggen of toch maar beter even kunt wachten totdat duidelijk wordt hoe we van het gas af gaan. 

<!--more-->

Wij vinden dat een moeilijke vraag, maar gelukkig heeft de gemeente Utrecht een groot team van deskundigen op het gebied van aardgasvrij, dus hebben we de vraag aan de gemeente voorgelegd. En die wist het eigenlijk ook niet. 
Op dit moment is Klopvaarbuurt Aardgasvrij betrokken bij een onderzoek naar een buurtwarmtenet voor onze buurt. Eén van de opties is water verwarmen met zonnecollectoren op het dak en dat warme water dan opslaan voor in de winter. Bij zo'n optie heb je dus daken nodig voor die zonnecollectoren. Een andere optie is PVT-panelen plaatsen, dat zijn zonnepanelen die zowel elektriciteit als warmte leveren. Ergens einde zomer zal duidelijk worden of deze opties ook echt kunnen gaan werken voor onze buurt. Dus je zou dat moment kunnen afwachten, voordat je je dak vol legt met zonnepanelen. Overigens kan het daarna nog jaren duren voordat ze aan de slag gaan in onze wijk. 

Maar isolatie speelt ook een rol: als er zonnepanelen op het dak liggen, zijn de isolatiekosten voor je dak wat hoger omdat deze voor het aanbrengen van isolatie weggehaald en daarna weer teruggeplaatst moeten worden. Op dit moment zijn we bezig met het uitzoeken van dakisolatie voor ons type woningen. Ook hier weten we rond de zomer meer van wat er mogelijk is en wat het gaat kosten en de planning is om voor de winter het isolatieproject voor onze buurt te starten. 

Dus ons advies: wacht het isolatieverhaal af en doe mee met de dakisolatie in de Klopvaartbuurt. Daarna is het een kwestie van rekenen: wat kost het leggen van panelen en in hoeveel jaar kan ik het terugverdienen? (zie hiervoor bijvoorbeeld [deze informatie van de Consumentenbond] (https://www.consumentenbond.nl/zonnepanelen/terugverdientijd-zonnepanelen#no2) ). Als die rekensom goed uitvalt  dan zouden wij na de dakisolatie die panelen gewoon laten installeren. Da’s ook nog eens beter voor het klimaat natuurlijk.
