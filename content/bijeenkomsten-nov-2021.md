---
title: "Bijeenkomsten Klopvaart Aardgasvrij: save the date"
date: 2021-11-06T07:51:38+02:00
url: "/bijeenkomsten-nov-2021"
image: "img/buurtavonden-nov-2021.png"
draft: false
description: "Al meer dan twee jaar zijn we als kernteam Klopvaartbuurt Aardgasvrij in gesprek met de gemeente over de beste aardgasvrije oplossing voor de Klopvaartbuurt.
Inmiddels heeft de gemeente op basis van onderzoek geconcludeerd dat stadsverwarming de beste oplossing is voor een groot deel van onze Klopvaartbuurt. Wat het kernteam betreft, zijn we nog niet overtuigd dat dit inderdaad de beste oplossing is en laten we een alternatief uitzoeken op haalbaarheid.
Hoe dan ook, we willen graag de stand van zaken met u delen en met u in gesprek gaan over stadsverwarming en mogelijke alternatieven.."

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Al meer dan twee jaar zijn we als kernteam Klopvaartbuurt Aardgasvrij in gesprek met de gemeente over de beste aardgasvrije oplossing voor de Klopvaartbuurt.
Inmiddels heeft de gemeente op basis van onderzoek geconcludeerd dat stadsverwarming de beste oplossing is voor een groot deel van onze Klopvaartbuurt. Wat het kernteam betreft, zijn we nog niet overtuigd dat dit inderdaad de beste oplossing is en laten we een alternatief uitzoeken op haalbaarheid.
Hoe dan ook, we willen graag de stand van zaken met u delen en met u in gesprek gaan over stadsverwarming en mogelijke alternatieven.

<!--more-->

In november verzorgen we 4 bijeenkomsten voor verschillende stukjes van onze buurt (zie het kaartje hieronder, 15, 17, 23 en 25 november). U krijgt begin november nog een uitnodiging op papier in de bus. Reserveer alvast een datum in uw agenda. De bijeenkomsten zijn in de stadstuin aan de Vancouverdreef en beginnen om 20 uur.

![Agenda bijeenkomsten per stuk van de Klopvaartbuurt](../img/buurtavonden-nov-2021.png "Agenda bijeenkomsten per stuk van de Klopvaartbuurt")

Mocht u echt niet kunnen op de avond van uw deel van de buurt, dan kunt u aanschuiven op een andere avond. Maar het is natuurlijk altijd leuker om met uw directe buren in gesprek te gaan.

Hopelijk zien en spreken we elkaar in november in de natuurtuin. Maar we vinden het ook fijn om nu al van zoveel mogelijk buren te horen wat jullie vinden over wat de gemeente wil met aardgasvrij en wat het kernteam daarvan vindt. Daarom vragen we u [deze enquête](https://docs.google.com/forms/d/e/1FAIpQLSd4SyzcGhN7WJ1EXNWTm31aeN9ntD5n8wr3zmGgbZvqduERuQ/viewform?pli=1&pli=1) in te vullen. We delen de resultaten tijdens onze bijeenkomsten in november. 
