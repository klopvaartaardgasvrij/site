---
title: "Onderzoek naar buurtwarmtenet"
date: 2021-04-25T16:51:38+02:00
url: "/onderzoek-naar-buurtwarmtenet"
image: "img/aansluiting-warmtenet-75-3.jpg"
draft: false
description: "Begin dit jaar is de gemeente gestart met een project met adviesbureau Greenvis. Onderdeel van dit project is het onderzoek naar de mogelijkheden voor een buurtwarmtenet. Bij dit onderdeel zijn beide buurtinitiatieven in Overvecht-Noord intensief betrokken."

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

Begin dit jaar is de gemeente gestart met een project met adviesbureau Greenvis. Onderdeel van dit project is het onderzoek naar de mogelijkheden voor een buurtwarmtenet. Bij dit onderdeel zijn beide buurtinitiatieven in Overvecht-Noord (Klopvaartbuurt Aargasvrij en Nieuwe Energie Vechtzoom) intensief betrokken. Zo zitten we ook aan tafel bij het tweewekelijkse projectteam overleg. 

<!--more-->

Als buurtinitiatieven hebben wij zo'n 12 interessante voorbeelden van buurtwarmtenetten in Nederland op tafel gelegd. Dit zijn voorbeelden die al gerealiseerd zijn of die vrij ver in de realisatie zijn. Greenvis heeft deze voorbeelden van data voorzien. Op basis van deze gegevens hebben we in overleg een keuze gemaakt om drie van die voorbeelden op haalbaarheid in onze buurten te onderzoeken: 

* [Nagele](https://www.energieknagele.nl/nagele-in-balans/): zonthermie met hogetemperatuur seizoensopslag, aanvoertemp. 70 ºC: in plaats van gewone zonnepanelen zonnecollectoren op het dak en de warmte zoveel mogelijk opslaan voor gebruik in de winter. 
* [Aquathermie](https://www.aquathermie.nl/default.aspx) met centrale warmtepomp, aanvoertemp. 70 ºC. Bij aquathermie onderzoeken we of we warmte uit de Vecht of Klopvaart kunnen gebruiken. Die warmte moet dan wel weer opgewaardeerd worden tot een temperatuur waarmee wij ons huis en ons tapwater kunnen verwarmen. 
* De Meent: gezamenlijke bodemlus per cluster woningen, elke woning een eigen bodemwarmtepomp. Eerst doen we een check of dit qua bodemgesteldheid geschikt is, mocht dit niet zo zijn dan vervangen we dit concept door [PVT-concept](https://blog.greenhome.nl/pvt-systeem/) van Ramplaankwartier in Haarlem (De Zonnet). 

Overigens, die bodemwarmtepomp is heel wat anders dan de beruchte luchtwarmtepomp die geluidsoverlast zou kunnen veroorzaken.

De volgende stap in het onderzoek is dat er een grof plaatje komt van wat het zou betekenen om zo'n oplossing in onze buurt te implementeren. Daarbij kijkt Greenvis naar kosten (en dus betaalbaarheid), benodigde ingrepen in het huis en duurzaamheid. Op basis van deze grove schets moeten we één oplossing kiezen die ons het meest haalbaar lijkt. Van die oplossing zal dan een businesscase worden opgesteld, zodat in detail duidelijk wordt waar de kosten zitten en wat dit betekent voor de betaalbaarheid.

Vanaf het begin is onze inzet dat uit het onderzoek duidelijkheid moet komen over hoe zo'n warmtealternatief scoort op [onze voorwaarden](https://klopvaartaardgasvrij.nl/burenactie/). We zullen het onderzoek dan ook kritisch blijven volgen. Mocht de uitgewerkte oplossing niet aan onze voorwaarden voldoen, dan zullen we verder moeten zoeken naar oplossing die dat wel doet. 