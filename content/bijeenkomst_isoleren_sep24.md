---
title: "Bijeenkomst isoleren september 2024"
date: 2024-10-05T07:51:38+02:00
url: "/bijeenkomst_isoleren_sep24"
image: "img/isolatie.png"
draft: false
description: "12 september 2024 vond een bijeenkomst plaats in de stadstuin aan de Vancouverdreef voor bewoners van de Klopvaartbuurt over isoleren. Het gaat hier om een gezamenlijke inkoopactie met Energie-U. De presentatie voor isolatiemaatregelen vind u hier. De presentatie over subsidiemogelijkheden vind u hier."

classes:
- feature-hyphenate
- feature-justif
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

12 september 2024 vond een bijeenkomst plaats in de stadstuin aan de Vancouverdreef voor bewoners van de Klopvaartbuurt over isoleren. Het gaat hier om een gezamenlijke inkoopactie met Energie-U. De presentatie voor isolatiemaatregelen vind u [hier](./files/20240912_klopvaart_PC.pdf). De presentatie over subsidiemogelijkheden vind u [hier](./files/Presentatie__isolatie_Klopvaart_sept_2024.pdf).