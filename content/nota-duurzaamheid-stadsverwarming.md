---
title: "Nota over duurzaamheid stadsverwarming"
date: 2022-09-12T08:51:38+02:00
url: "/nota-duurzaamheid-stadsverwarming"
image: "img/gesprek.png"
draft: false
description: "De gemeente werkt aan een plan om de stadsverwarming door te trekken naar de Klopvaartbuurt. Als kernteam zijn wij niet direct enthousiast over stadsverwarming. Eén van de pijnpunten betreft de duurzaamheid. Eneco heeft mooie verduurzamingsplannen voor de stadsverwarming. Samen met het kernteam Nieuwe Energie van de Vechtzoom (NEV, Costa Ricadreef e.o.) hebben wij als kernteam Klopvaartbuurt Aardgasvrij een nota opgesteld waarin we veel vraagtekens zetten bij de duurzaamheidsplannen van de Eneco. Hieronder deze nota. Binnenkort gaan we in gesprek met de gemeenteraad over onze visie op de duurzaamheid van de stadsverwarming."

classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Nieuws
---

De gemeente werkt aan een plan om de stadsverwarming door te trekken naar de Klopvaartbuurt. Als kernteam zijn wij niet direct enthousiast over stadsverwarming. Eén van de pijnpunten betreft de duurzaamheid. Eneco heeft mooie verduurzamingsplannen voor de stadsverwarming. Samen met het kernteam Nieuwe Energie van de Vechtzoom (NEV, Costa Ricadreef e.o.) hebben wij als kernteam Klopvaartbuurt Aardgasvrij een nota opgesteld waarin we veel vraagtekens zetten bij de duurzaamheidsplannen van de Eneco. Via de link hieronder kunt u onze nota vinden. Binnenkort gaan we in gesprek met de gemeenteraad over onze visie op de duurzaamheid van de stadsverwarming.

[Lees hier onze standpuntnota over de duurzaamheid van stadsverwarming](https://klopvaartaardgasvrij.gitlab.io/site/files/Duurzame_stadsverwarming_-_standpunt_nota_v1.0.pdf)
